## List Should Do For Your Computer

[Common](#common)

&nbsp;&nbsp;&nbsp;&nbsp;[• OhMyZsh](#ohmyzsh)

&nbsp;&nbsp;&nbsp;&nbsp;[• PHP](#php)

&nbsp;&nbsp;&nbsp;&nbsp;[• Laradock](#laradock)

&nbsp;&nbsp;&nbsp;&nbsp;[• Laravel](#laravel)

&nbsp;&nbsp;&nbsp;&nbsp;[• Docker](#docker)

&nbsp;&nbsp;&nbsp;&nbsp;[• Git](#git)

&nbsp;&nbsp;&nbsp;&nbsp;[• Firebase](#firebase)

&nbsp;&nbsp;&nbsp;&nbsp;[• MySQL](#mysql)

&nbsp;&nbsp;&nbsp;&nbsp;[• Nginx](#nginx)

&nbsp;&nbsp;&nbsp;&nbsp;[• Mongodb](#mongodb)

[Ubuntu](#ubuntu)

&nbsp;&nbsp;&nbsp;&nbsp;[• Ubuntu errors](#ubuntu-errors)

&nbsp;&nbsp;&nbsp;&nbsp;[• Ubuntu command](#ubuntu-command)

&nbsp;&nbsp;&nbsp;&nbsp;[• Ubuntu apps](#ubuntu-apps)

[Window](#window)

[MacOS](#macos)

## Common

#### OhMyZsh

```
sh -c "$(wget https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh -O -)"
```

##### _Set default zsh_

```
zsh -s $(which chsh)
```

#### PHP

##### _Set up_

```
sudo apt install php -y

sudo apt install composer nodejs -y

sudo apt install php-mysql php-gd openssl php-common php-curl php-json php-mbstring php-mysql php-xml php-zip
```

##### _Command artisan interesting_

```
php artisan inspire
```

##### _php config jwt_

```
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"

php artisan jwt:secret
```

##### _Run file php into server_

```
php -S localhost:port -t your_folder/
```

#### Laradock

```
git clone https://github.com/Laradock/laradock.git

cp .env.example .env

docker-compose up -d nginx mysql phpmyadmin
```

##### _Edit file .env_

> DB_HOST=mysql

#### Laravel

##### _Install project Laravel_

```
composer create-project --prefer-dist laravel/laravel {your_name_project}
```

```
git clone https://github.com/laravel/laravel.git laravel1
```

##### _Set up package laravel/ui_

```
composer require laravel/ui

php artisan ui vue --auth

npm install && npm run dev
```

##### _Set paper dashboard template_

```
composer require laravel-frontend-presets/paper

php artisan ui paper

composer dump-autoload
```

##### _Some package useful for laravel_

```
composer require intervention/image
```

#### Docker

```
sudo apt update
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"

sudo apt update
sudo apt install docker-ce -y
sudo usermod -aG docker ${USER}
su - ${USER}
```

##### _Docker-compose_

```
sudo curl -L "https://github.com/docker/compose/releases/download/1.29.1/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose

sudo chmod +x /usr/local/bin/docker-compose
```

#### Git

##### _Use token instead of origin_

```
https://CongMinh77:<Token>@<Link Git without https>
```

##### _Error clone https_

```
export GIT_SSL_NO_VERIFY=1
```

##### _Git config_

```
git config --global user.name "Minh Nguyen Cong"
git config --global user.email "cong.minh7.7.2000@gmail.com"
```

##### _Generate SSH key_

```
ssh-keygen -t rsa -b 2048 -C "email@example.com"

ssh-keygen -o -t rsa -b 4096 -C "email@example.com"
```

##### _Useful git commands_

###### Revert commit

```
git reset HEAD~1
```

###### _Rename branch_

```
git branch -m old-name new-name
```

###### Helpful git command

```
git pull origin branchname --allow-unrelated-histories
```

#### Firebase

```
firebase emulators:start --only functions,firestore --export-on-exit=./seeds --project {your_name_project_firebase}
```

#### MySQL

```
mysql.server start

mysql.server stop

mysql.server restart
```

##### _Command line create users_

```
CREATE DATABASE database_name;
CREATE USER 'user_name'@'%' IDENTIFIED WITH mysql_native_password BY 'password';
GRANT ALL ON database_name.* TO 'user_name'@'%';
FLUSH PRIVILEGES;
EXIT;
```

#### Nginx

```
sudo systemctl start nginx
```

##### _Test nginx_

```
sudo nginx -t
systemctl status nginx.service
```

#### Mongodb

login mongodb
```
mongosh admin -u 'username' -p 'password'
```

## Ubuntu

```
sudo apt-get update && sudo apt-get upgrate -y

sudo apt install wget git curl snapd zsh -y
```

#### Ubuntu errors

##### _Error full disk_

```
sudo sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=\"\(.*\)\"/GRUB_CMDLINE_LINUX_DEFAULT=\"\1 pcie_aspm=off\"/' /etc/default/grub && sudo update-grub
```

##### _Error grub-customizer_

```
sudo add-apt-repository --remove ppa:danielrichter2007/grub-customizer
```

##### _Error shows different time than window_

```
timedatectl set-local-rtc 1 --adjust-system-clock
```

#### Ubuntu command

##### _List port_

```
sudo ss -tunlp
```

##### _Kill port_

```
sudo kill $(sudo lsof -t -i:{port})
```

##### _Check version_

```
hostnamectl
```

##### _Remove app_

```
sudo apt-get --purge remove redis-server
```

#### Ubuntu apps

##### _Chrome_

```
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb

sudo dpkg -i google-chrome-stable_current_amd64.deb
```

##### _PHP Storm & Web Storm with snap_

```
sudo snap install phpstorm --classic
sudo snap install webstorm --classic
```

##### _Visual Studio Code_

```
sudo snap install code --classic
```

##### _Ibus bamboo (Vietkey)_

```
sudo add-apt-repository ppa:bamboo-engine/ibus-bamboo
sudo apt-get update
sudo apt-get install ibus-bamboo
ibus restart
```

## Window

##### _Error npm expo-cli_

```
Set-ExecutionPolicy -Scope Process -ExecutionPolicy Bypass

Set-ExecutionPolicy -Scope CurrentUser -ExecutionPolicy Unrestricted
```

##### _Battery report_

```
powercfg /batteryreport /output "C:\Users\USER\Desktop\batteryreport.html
```

## MacOS

##### _Xcode_

```
xcode-select --install
```

##### _Home brew_

```
/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install.sh)"
```

##### _Package install by brew_

```
brew analytics off

brew install node

brew install php@7.4

brew install zsh

brew install mysql@8.0

brew install watchman

brew install cocoapods

brew install rbenv

brew install stats
```

##### _Docker-compose_

```
docker-compose -f docker-compose.yml up
```

#### _List port_

```
sudo lsof -i -P | grep LISTEN | grep :$PORT
```

#### _Disable Gatekeeper & SIP_

```
sudo spctl --master-disable
```

```
csrutil disable
```

#### _Check status gatekeeper & SIP_

```
spctl --status
```

```
csrutil status
```
